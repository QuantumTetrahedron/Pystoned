import pyglet
from game import transform
import math
class Anim:
	def __init__(self,sprite,tr,dest,time,function,batch,):
		self.sprite = pyglet.sprite.Sprite(img = sprite,x=tr.position.x,y=tr.position.y,batch = batch)
		self.sprite.rotation = tr.rotation
		self.sprite.scale = tr.scale
		self.function = function
		self.time = time
		self.speed = [1,1,1,1]
		self.transform = tr
		self.dest = dest
		self.delete = False
		self.setSpeed()

	def setSpeed(self):
		self.speed[0] = (self.dest.position.x-self.transform.position.x)/(60.0*self.time)
		self.speed[1] = (self.dest.position.y-self.transform.position.y)/(60.0*self.time)
		self.speed[2] = (self.dest.rotation-self.transform.rotation)/(60.0*self.time)
		self.speed[3] = (self.dest.scale-self.transform.scale)/(60.0*self.time)

	def Update(self):
		if self.transform.position.x != self.dest.position.x:
			dx = self.speed[0]
		else:
			dx = 0
		if self.transform.position.x != self.dest.position.y:
			dy = self.speed[1]
		else:
			dy = 0
		if self.transform.rotation != self.dest.rotation:
			dr = self.speed[2]
		else:
			dr = 0
		if self.transform.scale != self.dest.scale:
			ds = self.speed[3]
		else:
			ds = 0


		self.transform.position.x += dx
		self.transform.position.y += dy
		self.transform.rotation += dr
		self.transform.scale += ds

		if (dx > 0 and self.transform.position.x > self.dest.position.x) or (dx<0 and self.transform.position.x < self.dest.position.x):
			self.transform.position.x = self.dest.position.x
		if (dy > 0 and self.transform.position.y > self.dest.position.y) or (dy<0 and self.transform.position.y < self.dest.position.y):
			self.transform.position.y = self.dest.position.y
		if (dr > 0 and self.transform.rotation > self.dest.rotation) or (dr<0 and self.transform.rotation < self.dest.rotation):
			self.transform.rotation = self.dest.rotation
		if (ds > 0 and self.transform.scale > self.dest.scale) or (ds<0 and self.transform.scale < self.dest.scale):
			self.transform.scale = self.dest.scale

		self.sprite.x = self.transform.position.x
		self.sprite.y = self.transform.position.y
		self.sprite.rotation = self.transform.rotation
		self.sprite.scale = self.transform.scale

		if self.transform == self.dest:
			self.function()
			self.delete = True