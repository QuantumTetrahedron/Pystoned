import pyglet
from game import player, resources, field, minimax

class GameController:
	def __init__(self):
		player.Player()
		player.Player()
		self.boardHistory = []
		self.board = []
		self.passCounter = 0
		self.winner = 0

	def Reload(self,ai):
		self.board = []
		self.boardHistory = []
		self.passCounter = 0
		self.winner = 0
		player.Player.players = []
		player.Player()
		if ai:
			player.Player(True, minimax.Minimax(5,2))
		else:
			player.Player()
		player.Player.activePlayer = 1

	def check(self):
		if self.passCounter == 2:
			return True
		return False

	def GetWinner(self):
		points = [0,0]
		for field in self.board:
			if field.value == 0:
				continue
			else:
				points[(field.value-1)%2]+=1
		if points[0]>points[1]:
			self.winner = 1
			return 1
		elif points[1]>points[0]:
			self.winner = 2
			return 2
		else:
			self.winner = 0
			return 0


	def LoadBoard(self,w,h,x,y,b):
		for i in range(h):
			for j in range(w):
				field_x = x+100*j
				field_y = y+100*i
				new_field = field.Field(img=resources.empty_image, x=field_x, y=field_y, batch=b)
				self.board.append(new_field)
		return self.board

	def SaveBoard(self):
		h = []
		for i in range(16):
			h.append(self.board[i].value)
		self.boardHistory.append(h)
		if len(self.boardHistory)>17:
			self.boardHistory.pop(0)

	def CheckTie(self):
		if len(self.boardHistory)<17:
			return False
		for i in range(3,18,2):
			for j in range(16):
				if self.board[j].value != self.boardHistory[-i][j]:
					return False
		return True

	def GetState(self):
		ret = []
		for field in self.board:
			ret.append(field.value)
		return ret