import pyglet
from game import resources
dragBatch = pyglet.graphics.Batch()
batch = pyglet.graphics.Batch()
lastBatch = pyglet.graphics.Batch()

class Field(pyglet.sprite.Sprite):
	def __init__(self, n = -1, canRotate = True, *args, **kwargs):
		super(Field, self).__init__(*args,**kwargs)
		self.static = False
		self.value = 0
		self.ext = False
		self.canRotate = canRotate
		self.n = n
		self.maxn = n

	def set(self, v, r = 0, cr = True):
		self.value = v
		self.rotation = r
		if v == 0:
			self.image = resources.empty_image
			self.batch = dragBatch
		else:
			self.image = resources.piston_images[v-1]
			self.batch = batch
			self.canRotate = cr
			if v == 7 or v == 8:
				self.static = True

	def InRect(self,x,y):
		if x>self.x - self.width/2 and x<self.x + self.width/2 and y>self.y - self.height/2 and y<self.y + self.height/2:
			return True
		return False

	def extend(self,b,p):
		if b:
			i = self.value+7
			self.batch = lastBatch
			self.ext = True
		else:
			i = self.value-1
			self.batch = batch
			self.ext = False
		self.image = resources.piston_images[i]

	def rotate(self, angle):
		if not self.canRotate:
			return
		self.rotation += angle
		while self.rotation >= 360:
			self.rotation -= 360

	def Use(self):
		if self.n == -1:
			return True
		else:
			if self.n == 0:
				return False
			else:
				self.n -=1
				if self.n == 0:
					self.opacity = 128
				return True

	def Reset(self):
		self.n = self.maxn
		self.opacity = 255