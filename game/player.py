import pyglet

class Player:
	players = []
	activePlayer=1

	def __init__(self, ai=False, minimax = None):
		Player.players.append(self)
		self.index = len(Player.players)-1
		self.ai = ai
		self.minimax = minimax

	def changePlayer():
		if Player.activePlayer == 1:
			Player.activePlayer=2
		else:
			Player.activePlayer=1

	def Active():
		return Player.players[Player.activePlayer-1]