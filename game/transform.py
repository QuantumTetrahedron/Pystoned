class Vector2:
	def __init__(self,v):
		self.x = v[0]
		self.y = v[1]

class Transform:
	def __init__(self,position,rotation,scale):
		self.position = Vector2(position)
		self.rotation = rotation
		self.scale = scale

	def __eq__(self,other):
		try:
			return self.position.x == other.position.x and self.position.y == other.position.y and self.rotation == other.rotation and self.scale == other.scale
		except:
			return False