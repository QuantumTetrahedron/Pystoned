import random

class Minimax:
	def __init__(self, depth, player):
		self.depth = depth
		self.counter = 0
		self.maxCounter = 3
		self.player = player


	def GetMove(self,state,passCounter,selector):
		arr=[]
		for i in range(16):
			for j in range(4):
				if state[i] == 0:
					x = j*2+self.player-1
					if selector[x]>0:
						state[i] = x+1
						selector[x] -=1
						arr.append((i,j,self.alphabeta(state,1,0,selector, -100000, 100000, False)))
						selector[x] +=1
						state[i] = 0
					else:
						continue
				elif state[i]%2 == self.player%2 and state[i]<7:
					newState, legitMove = self.Mov(state,i,j)
					if legitMove:
						arr.append((i,j,self.alphabeta(newState,1,0,selector, -100000, 100000, False)))
				else:
					break
		if passCounter == 0:
			arr.append((-1,0,self.alphabeta(state,1,1,selector, -100000, 100000, False)))
		elif passCounter == 1:
			x = self.EvaluateEnd(state,selector)
			if x>0:
				return (-1,0,90000)
			elif x==0:
				arr.append((-1,0,0))
			else:
				arr.append((-1,0,-90000))
		maximum = arr[0][2]
		for a in arr:
			if a[2] > maximum:
				maximum = a[2]
		retArr = []
		for a in arr:
			if a[2] == maximum:
				retArr.append(a)
		self.counter += 1
		if self.counter == self.maxCounter:
			self.counter = 0
			self.maxCounter-=1
			self.depth+=1
		if len(retArr) > 2:
			return retArr[random.randint(0,len(retArr)-(2 if retArr[-1][0]==-1 else 1))]
		return retArr[0]

	def alphabeta(self, state, d, passCounter, selector, alpha, beta, maximizing):
		if d >= self.depth:
			return self.Evaluate(state,d,selector)
		if maximizing:
			v = -100000
			if self.EvaluateExt(state,selector)<alpha:
				return v
			if passCounter == 0:
				v = max(v,self.alphabeta(state,d+1,1,selector,alpha,beta,False))
				alpha = max(alpha,v)
				if beta <= alpha:
					return v
			elif passCounter == 1:
				x = self.EvaluateEnd(state,selector)
				if x>0:
					return 90000+d
				elif x==0:
					v = max(v,d)
				else:
					v = max(v, -90000+d)
				alpha = max(alpha,v)
				if beta <= alpha:
					return v
			for i in range(16):
				b = False
				for j in range(4):
					if state[i]==0:
						x = j*2+self.player-1
						if selector[x]>0:
							state[i] = x+1
							selector[x] -= 1
							v = max(v, self.alphabeta(state, d+1,0,selector,alpha,beta,False))
							alpha = max(alpha, v)
							selector[x] +=1
							state[i] = 0
							if beta <= alpha:
								b = True
								break
						else:
							continue
					elif state[i]%2 == self.player%2 and state[i]<7:
						newState, legitMove = self.Mov(state,i,j)
						if legitMove:
							v = max(v,self.alphabeta(newState,d+1,0,selector,alpha,beta,False))
							alpha = max(alpha,v)
							if beta <= alpha:
								b = True
								break
				if b:
					break
			return v
		else:
			v = 100000
			if passCounter == 0:
				v = min(v, self.alphabeta(state,d+1,1,selector,alpha,beta,True))
				beta = min(beta,v)
				if beta <= alpha:
					return v
			elif passCounter == 1:
				x = self.EvaluateEnd(state,selector)
				if x<0:
					return -90000+d
				elif x==0:
					v = min(v,d)
				else:
					v = min(v,90000+d)
				beta = min(beta,v)
				if beta <= alpha:
					return v
			for i in range(16):
				b = False
				for j in range(4):
					if state[i] == 0:
						x = j*2+self.player%2
						if selector[x]>0:
							state[i] = x+1
							selector[x]-=1
							v = min(v,self.alphabeta(state,d+1,0,selector,alpha,beta,True))
							beta = min(beta,v)
							selector[x]+=1
							state[i] = 0
							if beta<=alpha:
								b = True
								break
						else:
							continue
					elif state[i]%2 == (self.player-1)%2 and state[i]<7:
						newState, legitMove = self.Mov(state,i,j)
						if legitMove:
							v = min(v,self.alphabeta(newState,d+1,0,selector,alpha,beta,True))
							beta = min(beta, v)
							if beta <= alpha:
								b = True
								break
				if b:
					break
			return v

	def EvaluateExt(self,state,selector):
		score = 0
		for i in range(16):
			if state[i]==0:
				continue
			if state[i]%2==self.player%2:
				score+=100
			else:
				score-=100
		for s in range(self.player-1,8,2):
			score+=100*selector[s]
		for s in range(self.player%2,8,2):
			score-=100*selector[s]
		return score

	def Evaluate(self,state,d,selector):
		score = 0
		arr = [0,0,0,0,0,0,0,0]
		arr2 = [5,5,1,1,1,1,1,1]
		for i in range(16):
			if state[i]==0:
				continue
			if state[i]%2 == self.player%2:
				score+=100
				arr[state[i]-1]+=1
			else:
				score-=100
				arr[state[i]-1]+=1
		for s in range(8):
			score+=((-1)**(s+self.player))*1000*(arr2[s]-arr[s]-selector[s])
		return score+d

	def EvaluateEnd(self,state,selector):
		score = 0
		for i in range(16):
			if state[i]==0:
				continue
			if state[i]%2==self.player%2:
				score+=100
			else:
				score-=100
		return score

	def Mov(self, state, field, rot):
		if state[field] == 0:
			return state, False
		newState = state[:]
		directions = []
		directions.append(rot)
		if state[field] == 3 or state[field] == 4:
			if rot == 3:
				directions.append(0)
			else:
				directions.append(rot+1)
		elif state[field] == 5 or state[field] == 6: 
			if rot < 2:
				directions.append(rot+2)
			else:
				directions.append(rot-2)

		for d in directions:
			if d == 0 and field<12 and (state[field+4]>=7 or (field>=8 and state[field+4] and state[field+4]%2 == state[field]%2) or (state[field+4] and field<8 and state[field+8])):
				return state, False
			if d == 1 and (field+1)%4!=0 and (state[field+1]>=7 or ((field+2)%4==0 and state[field+1] and state[field+1]%2 == state[field]%2) or (state[field+1] and (field+2)%4!=0 and state[field+2])):
				return state, False
			if d == 2 and field>=4 and (state[field-4]>=7 or (field<8 and state[field-4] and state[field-4]%2 == state[field]%2) or (state[field-4] and field>=8 and state[field-8])):
				return state, False
			if d == 3 and field%4!=0 and (state[field-1]>=7 or ((field-1)%4==0 and state[field-1] and state[field-1]%2 == state[field]%2) or (state[field-1] and (field-1)%4!=0 and state[field-2])):
				return state, False

		canMove = False
		for d in directions:
			if d == 0 and field<12 and state[field+4]:
				canMove = True
				if field<8:
					newState[field+8] = newState[field+4]
				newState[field+4] = 0
			elif d == 1 and (field+1)%4!=0 and state[field+1]:
				canMove = True
				if (field+2)%4!=0:
					newState[field+2] = newState[field+1]
				newState[field+1] = 0
			elif d == 2 and field >= 4 and state[field-4]:
				canMove = True
				if field >= 8:
					newState[field-8] = newState[field-4]
				newState[field-4] = 0
			elif d == 3 and field%4!=0 and state[field-1]:
				canMove = True
				if (field-1)%4!=0:
					newState[field-2] = newState[field-1]
				newState[field-1] = 0	
		return newState, canMove