import pyglet
pyglet.options['audio'] = ('openal', 'silent')
pyglet.resource.path=["resources"]
pyglet.resource.reindex()

piston_images = []
piston_images.append(pyglet.resource.image("pistons/piston1.png"))
piston_images.append(pyglet.resource.image("pistons/piston2.png"))
piston_images.append(pyglet.resource.image("pistons/piston3.png"))
piston_images.append(pyglet.resource.image("pistons/piston4.png"))
piston_images.append(pyglet.resource.image("pistons/piston5.png"))
piston_images.append(pyglet.resource.image("pistons/piston6.png"))
piston_images.append(pyglet.resource.image("pistons/piston7.png"))
piston_images.append(pyglet.resource.image("pistons/piston8.png"))
piston_images.append(pyglet.resource.image("pistons/piston1ext.png"))
piston_images.append(pyglet.resource.image("pistons/piston2ext.png"))
piston_images.append(pyglet.resource.image("pistons/piston3ext.png"))
piston_images.append(pyglet.resource.image("pistons/piston4ext.png"))
piston_images.append(pyglet.resource.image("pistons/piston5ext.png"))
piston_images.append(pyglet.resource.image("pistons/piston6ext.png"))
piston_images.append(pyglet.resource.image("pistons/piston7.png"))
piston_images.append(pyglet.resource.image("pistons/piston8.png"))

background_image = pyglet.resource.image("Background.png")
background_image2 = pyglet.resource.image("Background2.png")
end_turn_image = pyglet.resource.image("EndTurn.png")
empty_image = pyglet.resource.image("empty.png")

main_screen = pyglet.resource.image("mainMenu.png")
title = pyglet.resource.image("Title.png")
new_game = pyglet.resource.image("NewGame.png")
quit_game = pyglet.resource.image("QuitGame.png")
options = pyglet.resource.image("Options.png")
how_to = pyglet.resource.image("HowTo.png")

OneP = pyglet.resource.image("1P.png")
TwoP = pyglet.resource.image("2P.png")
Back = pyglet.resource.image("Back.png")

MusicOn = pyglet.resource.image("MusicOn.png")
MusicOff = pyglet.resource.image("MusicOff.png")
SoundsOn = pyglet.resource.image("SoundOn.png")
SoundsOff = pyglet.resource.image("SoundOff.png")

HowTo1 = pyglet.resource.image("HowToScreen1.png")
HowTo2 = pyglet.resource.image("HowToScreen2.png")
Next = pyglet.resource.image("Next.png")
Prev = pyglet.resource.image("Prev.png")

Xwin = pyglet.resource.image("Xwin.png")
Owin = pyglet.resource.image("Owin.png")
Draw = pyglet.resource.image("Draw.png")
Pause = pyglet.resource.image("Pause.png")
OptionsMenu = pyglet.resource.image("OptionsMenu.png")

P1Start = pyglet.resource.image("Player1Start.png")
P2Start = pyglet.resource.image("Player2Start.png")
P2AI = pyglet.resource.image("Player2AI.png")

missing_image = pyglet.resource.image("empty.png")

menusdtr = pyglet.resource.media("Mining by Moonlight.mp3")
gamesdtr = pyglet.resource.media("Thief in the Night.mp3")

buttonSnd = pyglet.resource.media('Button.mp3', streaming=False)

def center_image(image, w=-1,h=-1):
	if w == -1:
		w = image.width
	if h == -1:
		h = image.height
	image.anchor_x = w/2
	image.anchor_y = h/2

for i in piston_images:
	center_image(i, 100, 100)
center_image(piston_images[12])
center_image(piston_images[13])
center_image(background_image)
center_image(background_image2)
center_image(end_turn_image)
center_image(empty_image)
center_image(missing_image)
center_image(main_screen)
center_image(new_game)
center_image(quit_game)
center_image(options)
center_image(how_to)
center_image(title)
center_image(Xwin)
center_image(Owin)
center_image(Draw)
center_image(Pause)
center_image(OptionsMenu)
center_image(OneP)
center_image(TwoP)
center_image(Back)
center_image(MusicOn)
center_image(MusicOff)
center_image(SoundsOn)
center_image(SoundsOff)
center_image(HowTo1)
center_image(HowTo2)
center_image(Next)
center_image(Prev)
center_image(P1Start)
center_image(P2Start)
center_image(P2AI)