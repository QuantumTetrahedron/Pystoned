import pyglet
from game import transform,resources

class Button:

	def __init__(self,tr,sprite,function, batch, animate=False,dest=None,speed = 0, sound = True):
		self.sprite = pyglet.sprite.Sprite(img = sprite,x = tr.position.x, y = tr.position.y, batch = batch)
		self.sprite.rotation = tr.rotation
		self.sprite.scale = tr.scale
		self.function = function
		self.animate = animate
		self.speed = speed
		self.transform = tr
		self.origin = transform.Transform((tr.position.x,tr.position.y),tr.rotation,tr.scale)
		self.target = transform.Transform((tr.position.x,tr.position.y+20),0,1)
		self.sound = sound
		self.ignoreInput = False
		if self.animate and dest is not None:
			self.target = dest
		self.dest = self.origin

	def onPress(self,snd):
		if self.sound and snd:
			resources.buttonSnd.play()
		if not self.ignoreInput:
			self.function()

	def InRect(self, x, y):
		if x>self.transform.position.x - self.sprite.width/2 and x<self.transform.position.x + self.sprite.width/2 and y>self.transform.position.y - self.sprite.height/2 and y<self.transform.position.y + self.sprite.height/2:
			return True
		return False

	def Hover(self,h):
		if h and not self.ignoreInput:
			self.dest = self.target
		else:
			self.dest = self.origin

	def Update(self):
		if not self.animate:
			return
		if self.transform.position.x != self.dest.position.x:
			dx = self.speed if self.dest.position.x-self.transform.position.x >0 else -self.speed
		else:
			dx = 0
		if self.transform.position.x != self.dest.position.y:
			dy = self.speed if self.dest.position.y-self.transform.position.y >0 else -self.speed
		else:
			dy = 0
		if self.transform.rotation != self.dest.rotation:
			dr = self.speed if self.dest.rotation-self.transform.rotation > 0 else -self.speed
		else:
			dr = 0
		if self.transform.scale != self.dest.scale:
			ds = self.speed/100.0 if self.dest.scale-self.transform.scale > 0 else -self.speed/100.0
		else:
			ds = 0


		self.transform.position.x += dx
		self.transform.position.y += dy
		self.transform.rotation += dr
		self.transform.scale += ds

		if (dx > 0 and self.transform.position.x > self.dest.position.x) or (dx<0 and self.transform.position.x < self.dest.position.x):
			self.transform.position.x = self.dest.position.x
		if (dy > 0 and self.transform.position.y > self.dest.position.y) or (dy<0 and self.transform.position.y < self.dest.position.y):
			self.transform.position.y = self.dest.position.y
		if (dr > 0 and self.transform.rotation > self.dest.rotation) or (dr<0 and self.transform.rotation < self.dest.rotation):
			self.transform.rotation = self.dest.rotation
		if (ds > 0 and self.transform.scale > self.dest.scale) or (ds<0 and self.transform.scale < self.dest.scale):
			self.transform.scale = self.dest.scale

		self.sprite.x = self.transform.position.x
		self.sprite.y = self.transform.position.y
		self.sprite.rotation = self.transform.rotation
		self.sprite.scale = self.transform.scale