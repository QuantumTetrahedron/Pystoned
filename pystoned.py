import pyglet
import math
import time
from pyglet.window import mouse
from game import resources, field, player, button, game_controller, minimax, transform, animation
from enum import Enum

class State(Enum):
	MainMenu = 1
	InGame = 2
	GameOver = 3
	Paused = 4
	Options = 5
	HowTo = 6
state = State.MainMenu

class Dir(Enum):
	Up = 0
	Right = 1
	Down = 2
	Left = 3
	def __int__(self):
		return self.value

menuButtons = pyglet.graphics.Batch()
optButtons = pyglet.graphics.Batch()
howToButtons = pyglet.graphics.Batch()
batch = pyglet.graphics.Batch()
dragBatch = pyglet.graphics.Batch()
animBatch = pyglet.graphics.Batch()

game_window = pyglet.window.Window(width=800, height=600)
gamectrl = game_controller.GameController()

WIDTH = 4
HEIGHT = 4

optionsScreen = pyglet.sprite.Sprite(img = resources.OptionsMenu,x = game_window.width/2,y = game_window.height/2)
HowToScreens = []
HowToScreens.append(pyglet.sprite.Sprite(img=resources.HowTo1,x = game_window.width/2,y=game_window.height/2))
HowToScreens.append(pyglet.sprite.Sprite(img=resources.HowTo2,x = game_window.width/2,y=game_window.height/2))
mainScreen = pyglet.sprite.Sprite(img = resources.main_screen, x = game_window.width/2, y = game_window.height/2)
title = pyglet.sprite.Sprite(img = resources.title, x = game_window.width/2, y = game_window.height/2, batch = menuButtons)
gameOverScreen = []
gameOverScreen.append(pyglet.sprite.Sprite(img = resources.Draw, x=game_window.width/2, y=game_window.height/2))
gameOverScreen.append(pyglet.sprite.Sprite(img = resources.Xwin, x=game_window.width/2, y=game_window.height/2))
gameOverScreen.append(pyglet.sprite.Sprite(img = resources.Owin, x=game_window.width/2, y=game_window.height/2))
pauseScreen = pyglet.sprite.Sprite(img = resources.Pause, x=game_window.width/2, y=game_window.height/2)

menuloop = pyglet.media.SourceGroup(resources.menusdtr.audio_format, None)
menuloop.loop = True
menuloop.queue(resources.menusdtr)
gameloop = pyglet.media.SourceGroup(resources.gamesdtr.audio_format, None)
gameloop.loop = True
gameloop.queue(resources.gamesdtr)
p = pyglet.media.Player()

Music = True
Sounds = True

def ToggleMusic():
	global Music
	if Music:
		Music = False
		optB[1].sprite.image = resources.MusicOff
		p.volume = 0
	else:
		Music = True
		optB[1].sprite.image = resources.MusicOn
		p.volume = 0.75

def ToggleSounds():
	global Sounds
	if Sounds:
		Sounds = False
		optB[2].sprite.image = resources.SoundsOff
	else:
		Sounds = True
		optB[2].sprite.image = resources.SoundsOn

dirSelector =[]
for i in range(8):
	dirSelector.append(field.Field(img=resources.piston_images[i], x=game_window.width*(i%2)+((-1)**i)*75, y=180+80*(i//2), batch=batch,n=(i//2+3)%4+2-(i//2)))
	if i > 5:
		dirSelector[i].canRotate = False
for d in dirSelector:
	d.scale = 64/100.0

draggedField = None
destRot = 0
selector = None

back = pyglet.sprite.Sprite(img=resources.background_image, x = game_window.width/2, y = game_window.height/2)
back2 = pyglet.sprite.Sprite(img=resources.background_image2, x = game_window.width/2, y = game_window.height/2)
back2.rotation = 180
turnEnded=False

def EndTurn(p = False):
	global turnEnded
	global state
	if p:
		gamectrl.passCounter+=1
	else:
		gamectrl.passCounter=0
	turnEnded = True
	if gamectrl.check():
		gamectrl.GetWinner()
		state = State.GameOver
AIfield = -1
AIvalue = -1
def AIMove():
	global anim
	global turnEnded
	fields[AIfield].set(AIvalue*2+player.Player.activePlayer)
	dirSelector[AIvalue*2+player.Player.activePlayer-1].Use()
	anim = None
	EndTurn()
	turnEnded = False
	StartTurn()

def StartTurn(first = False):
	global anim
	if player.Player.activePlayer == 2 or first:
		anim = animation.Anim(resources.P1Start,transform.Transform((game_window.width//2,game_window.height//2),0,0),transform.Transform((game_window.width//2,game_window.height//2),0,1),1,lambda: StartTurnAnim(first),animBatch)
	else:
		if player.Player.players[1].ai:
			anim = animation.Anim(resources.P2AI,transform.Transform((game_window.width//2,game_window.height//2),0,0),transform.Transform((game_window.width//2,game_window.height//2),0,1),1,lambda: StartTurnAnim(first),animBatch)
		else:
			anim = animation.Anim(resources.P2Start,transform.Transform((game_window.width//2,game_window.height//2),0,0),transform.Transform((game_window.width//2,game_window.height//2),0,1),1,lambda: StartTurnAnim(first),animBatch)


def StartTurnAnim(first = False):
	global AIfield
	global AIvalue
	global turnEnded
	global anim
	anim = None
	if not first:
		player.Player.changePlayer()
	gamectrl.SaveBoard()
	if gamectrl.CheckTie():
		gamectrl.passCounter = 1
	if player.Player.Active().ai:
		UpdateUI()
		sel = []
		for s in dirSelector:
			sel.append(s.n)
		AIfield, AIvalue, score = player.Player.Active().minimax.GetMove(gamectrl.GetState(),gamectrl.passCounter, sel)
		if AIfield == -1:
			Pass()
			turnEnded = False
			StartTurn()
		else:
			if fields[AIfield].value == 0:
				anim = animation.Anim(resources.piston_images[AIvalue*2+1], transform.Transform((game_window.width-75,180+80*AIvalue),0,64.0/100), transform.Transform((game_window.width/2-150+100*(AIfield%4),game_window.height/2-220+100*(AIfield//4)),0,1),1,AIMove,animBatch)
			else:
				while fields[AIfield].rotation != AIvalue * 90:
					fields[AIfield].rotate(90)
				if fields[AIfield].value < 3:
					Mov(AIfield, Dir((int(Dir.Up)+fields[AIfield].rotation/90)%4))
				elif fields[AIfield].value < 5:
					Mov(AIfield, Dir((int(Dir.Up)+fields[AIfield].rotation/90)%4), Dir((int(Dir.Right)+fields[AIfield].rotation/90)%4))
				elif fields[AIfield].value < 7:
					Mov(AIfield, Dir((int(Dir.Up)+fields[AIfield].rotation/90)%4), Dir((int(Dir.Down)+fields[AIfield].rotation/90)%4))
				fields[AIfield].extend(True,player.Player.activePlayer)
				EndTurn()
				turnEnded = False
				StartTurn()
	UpdateUI()


def StartGame(ai):
	global state
	global fields
	global turnEnded
	pyglet.clock.unschedule(MenuUpdate)
	pyglet.clock.schedule_interval(GameUpdate,1/60.0)
	state = State.InGame
	turnEnded = False
	gamectrl.Reload(ai)
	fields = gamectrl.LoadBoard(WIDTH,HEIGHT,game_window.width/2-150,game_window.height/2-220,field.dragBatch)
	for d in dirSelector:
		d.Reset()
	UpdateUI()
	p.queue(gameloop)
	p.next()
	if Music:
		p.volume = 0.5
	else:
		p.volume = 0
	StartTurn(True)

def Start1p():
	StartGame(True)

def Start2p():
	StartGame(False)

startSeq = 0
def StartSeq():
	global startSeq
	startSeq = 1
	for b in menuB1:
		b.ignoreInput = True
		b.origin.position.y = -100
		b.speed = 10
		b.animate = True
		b.Hover(False)

def StartSeq2():
	global startSeq
	startSeq = 2
	for b in menuB2:
		b.origin.position.y = 200
		b.speed = 10
		b.animate = True
		b.ignoreInput = True
		b.Hover(False)

def StartSeq3():
	global startSeq
	startSeq = 3
	for b in menuB2:
		b.speed = 2
		b.ignoreInput = False

def StartSeq4():
	global startSeq
	startSeq = 4
	for b in menuB2:
		b.ignoreInput = True
		b.origin.position.y = -100
		b.speed = 10
		b.animate = True
		b.Hover(False)

def StartSeq5():
	global startSeq
	startSeq = 5
	for b in menuB1:
		b.origin.position.y = 200
		b.speed = 10
		b.animate = True
		b.ignoreInput = True
		b.Hover(False)

def StartSeq0():
	global startSeq
	startSeq = 0
	for b in menuB1:
		b.speed = 2
		b.ignoreInput = False

def EnterMain():
	global state
	global startSeq
	startSeq = 0
	for b in menuB1:
		b.origin.position.y = 200
		b.transform.position.y = 200
		b.speed = 2
		b.ignoreInput = False
		b.Hover(False)
	for b in menuB2:
		b.origin.position.y = -100
		b.transform.position.y = -100
		b.ignoreInput = True
		b.Hover(False)
	pyglet.clock.unschedule(GameUpdate)
	pyglet.clock.schedule_interval(MenuUpdate, 1/60.0)
	state = State.MainMenu
	p.queue(menuloop)
	if p.playing:
		p.next()
	else:
		p.play()
	if Music:
		p.volume = 0.75
	else:
		p.volume = 0

def TogglePause():
	global state
	if state == State.InGame:
		state = State.Paused
	elif state == State.Paused:
		state = State.InGame

def ToggleOptions():
	global state
	if state == State.MainMenu:
		state = State.Options
	elif state == State.Options:
		state = State.MainMenu

HowToScreen = 0
def ToggleHowTo():
	global state
	global HowToScreen
	if state == State.MainMenu:
		state = State.HowTo
		HowToScreen = 0
	elif state == State.HowTo:
		state = State.MainMenu

def SwitchPage():
	global HowToScreen
	if HowToScreen == 0:
		HowToScreen = 1
		HowToB[0].sprite.image = resources.Prev
	else:
		HowToScreen = 0
		HowToB[0].sprite.image = resources.Next

def QuitGame():
	pyglet.app.exit()

def Pass():
	EndTurn(True)

endTurnButton = button.Button(transform.Transform((game_window.width/2,game_window.height-80),0,0.75),resources.end_turn_image, Pass, batch, True, transform.Transform((game_window.width/2,game_window.height-80),0,0.9),1)
menuB1 = []
menuB1.append(button.Button(transform.Transform((284,200),0,1),resources.new_game,StartSeq,menuButtons,True,transform.Transform((284,220),0,1),2))
menuB1.append(button.Button(transform.Transform((516,200),0,1),resources.quit_game,QuitGame,menuButtons,True,transform.Transform((516,220),0,1),2))
menuB1.append(button.Button(transform.Transform((104,200),0,1),resources.options,ToggleOptions,menuButtons,True,transform.Transform((104,220),0,1),2))
menuB1.append(button.Button(transform.Transform((696,200),0,1),resources.how_to,ToggleHowTo,menuButtons,True,transform.Transform((696,220),0,1),2))
menuB2 = []
menuB2.append(button.Button(transform.Transform((284,-100),0,1),resources.OneP,Start1p,menuButtons,True,transform.Transform((284,220),0,1),2))
menuB2.append(button.Button(transform.Transform((516,-100),0,1),resources.TwoP,Start2p,menuButtons,True,transform.Transform((516,220),0,1),2))
menuB2.append(button.Button(transform.Transform((104,-100),0,1),resources.Back,StartSeq4,menuButtons,True,transform.Transform((104,220),0,1),2))

optB = []
optB.append(button.Button(transform.Transform((700,500),0,0.5),resources.quit_game,ToggleOptions,optButtons,True,transform.Transform((700,500),0,0.6),1))
optB.append(button.Button(transform.Transform((284,240),0,1),resources.MusicOn,ToggleMusic,optButtons,True,transform.Transform((284,260),0,1),2))
optB.append(button.Button(transform.Transform((516,240),0,1),resources.SoundsOn,ToggleSounds,optButtons,True,transform.Transform((516,260),0,1),2))

HowToB = []
HowToB.append(button.Button(transform.Transform((700,100),0,1),resources.Next,SwitchPage,howToButtons,True,transform.Transform((700,100),0,1.1),1))
HowToB.append(button.Button(transform.Transform((700,500),0,0.5),resources.quit_game,ToggleHowTo,howToButtons,True,transform.Transform((700,500),0,0.6),1))

anim = None

def UpdateUI():
	if player.Player.activePlayer == 1:
		back2.rotation = 180
	else:
		back2.rotation = 0
	if gamectrl.passCounter > 0:
		endTurnButton.sprite.color = [255,50,50]
	else:
		endTurnButton.sprite.color = [255,255,255]

def Mov(i,*args):
	spend = False
	for arg in args:
		if arg == Dir.Up and i+WIDTH<len(fields) and (fields[i+WIDTH].static or (fields[i+WIDTH].value and i+2*WIDTH<len(fields) and fields[i+2*WIDTH].value)):
			return False, False
		if arg == Dir.Right and (i+1)%WIDTH!=0 and (fields[i+1].static or (fields[i+1].value and (i+2)%WIDTH!=0 and fields[i+2].value)):
			return False, False
		if arg == Dir.Down and i-WIDTH>=0 and (fields[i-WIDTH].static or (fields[i-WIDTH].value and i-2*WIDTH>=0 and fields[i-2*WIDTH].value)):
			return False, False
		if arg == Dir.Left and i%WIDTH!=0 and (fields[i-1].static or (fields[i-1].value and (i-1)%WIDTH!=0 and fields[i-2].value)):
			return False, False

	for arg in args:
		if arg == Dir.Up and i+WIDTH<len(fields) and fields[i+WIDTH].value!=0:
			spend = True
			if i+2*WIDTH<len(fields):
				fields[i+2*WIDTH].set(fields[i+WIDTH].value, fields[i+WIDTH].rotation)
			fields[i+WIDTH].set(0)
		elif arg == Dir.Right and (i+1)%WIDTH!=0 and fields[i+1].value!=0:
			spend = True
			if (i+2)%WIDTH!=0:
				fields[i+2].set(fields[i+1].value, fields[i+1].rotation)
			fields[i+1].set(0)
		elif arg == Dir.Down and i-WIDTH>=0 and fields[i-WIDTH].value!=0:
			spend = True
			if i-2*WIDTH>=0:
				fields[i-2*WIDTH].set(fields[i-WIDTH].value, fields[i-WIDTH].rotation)
			fields[i-WIDTH].set(0)
		elif arg == Dir.Left and i%WIDTH!=0 and fields[i-1].value!=0:
			spend = True
			if (i-1)%WIDTH!=0:
				fields[i-2].set(fields[i-1].value, fields[i-1].rotation)
			fields[i-1].set(0)
	return spend, True

@game_window.event
def on_mouse_motion(x,y,dx,dy):
	if state == State.InGame:
		endTurnButton.Hover(endTurnButton.InRect(x,y))

	elif state == State.MainMenu:
		for b in menuB1:
			b.Hover(b.InRect(x,y))
		for b in menuB2:
			b.Hover(b.InRect(x,y))
	elif state == State.Options:
		for b in optB:
			b.Hover(b.InRect(x,y))
	elif state == State.HowTo:
		for b in HowToB:
			b.Hover(b.InRect(x,y))

@game_window.event
def on_mouse_press(x, y, button, modifiers):
	global draggedField
	global selector
	global actionPoints
	global turnEnded
	if state == State.MainMenu:
		for b in menuB1:
			if button == mouse.LEFT and b.InRect(x,y):
				b.onPress(Sounds)
		for b in menuB2:
			if button == mouse.LEFT and b.InRect(x,y):
				b.onPress(Sounds)
		return
	if state == State.Options:
		for b in optB:
			if button == mouse.LEFT and b.InRect(x,y):
				b.onPress(Sounds)
		return

	if state == State.HowTo:
		for b in HowToB:
			if button == mouse.LEFT and b.InRect(x,y):
				b.onPress(Sounds)
		return
	if state == State.InGame:
		for i in range(0,len(dirSelector),2):
			d = dirSelector[i+player.Player.activePlayer-1]
			if button == mouse.LEFT:
				if d.InRect(x,y) and d.n!=0:
					draggedField = field.Field(img=d.image, x = x, y = y, batch = dragBatch)
					draggedField.rotation = d.rotation
					draggedField.value = i+player.Player.activePlayer
					draggedField.scale = 64/100.0
					draggedField.canRotate = d.canRotate
					selector = d
		
		if endTurnButton.InRect(x,y) and button == mouse.LEFT:
			endTurnButton.onPress(Sounds)

		i=-1
		for fld in fields:
			i+=1
			if fld.InRect(x,y):
				if fld.value!=0 and fld.value%2 == player.Player.activePlayer%2:
					if button == mouse.LEFT:
						ext = False
						spend = False
						if fld.value < 3:
							spend, ext = Mov(i, Dir((int(Dir.Up)+fld.rotation/90)%4))
						elif fld.value < 5:
							spend, ext = Mov(i, Dir((int(Dir.Up)+fld.rotation/90)%4), Dir((int(Dir.Right)+fld.rotation/90)%4))
						elif fld.value < 7:
							spend, ext = Mov(i, Dir((int(Dir.Up)+fld.rotation/90)%4), Dir((int(Dir.Down)+fld.rotation/90)%4))
						else:
							break
						if ext:
							fld.extend(True,player.Player.activePlayer)
						if spend:
							gamectrl.passCounter=0
							EndTurn()
						break
					elif button == mouse.RIGHT and not fld.ext:
						fld.rotate(90)
					
@game_window.event
def on_mouse_release(x, y, button, modifiers):
	global turnEnded
	global draggedField
	if state != State.InGame:
		return
	if draggedField is not None:
		for field in fields:
			if field.InRect(x,y) and button == mouse.LEFT and field.value == 0:
				field.set(draggedField.value, 0, draggedField.canRotate)
				selector.Use()
				EndTurn()
				break
	draggedField = None

	for field in fields:
		if field.ext:
			field.extend(False, player.Player.activePlayer)

	if turnEnded:
		turnEnded = False
		StartTurn()
	UpdateUI()

@game_window.event
def on_mouse_drag(x, y, dx, dy, button, modifiers):
	global destRot
	if state != State.InGame:
		return
	if draggedField is not None and button == mouse.LEFT:
		draggedField.x = x
		draggedField.y = y
		if dx>0:
			destRot = 10
		elif dx<0:
			destRot = -10

@game_window.event
def on_key_press(symbol, modifiers):
	global state
	if (state == State.InGame or state == State.Paused) and symbol == pyglet.window.key.ESCAPE:
		TogglePause()
		return pyglet.event.EVENT_HANDLED
	if state == State.Options and symbol == pyglet.window.key.ESCAPE:
		ToggleOptions()
		return pyglet.event.EVENT_HANDLED

	if state == State.HowTo and symbol == pyglet.window.key.ESCAPE:
		ToggleHowTo()
		return pyglet.event.EVENT_HANDLED

	if state == State.GameOver:
		EnterMain()


@game_window.event
def on_draw():
	game_window.clear()
	if state == State.MainMenu or state == State.Options or state == State.HowTo:
		mainScreen.draw()
		menuButtons.draw()
	elif state == State.InGame or state == State.GameOver or state == State.Paused:
		back2.draw()
		if draggedField is not None:
			field.dragBatch.draw()
		field.batch.draw()
		field.lastBatch.draw()
		back.draw()
		batch.draw()
		dragBatch.draw()
		animBatch.draw()
	if state == State.GameOver:
		gameOverScreen[gamectrl.winner].draw()
	if state == State.Paused:
		pauseScreen.draw()
	if state == State.Options:
		optionsScreen.draw()
		optButtons.draw()
	if state == State.HowTo:
		HowToScreens[HowToScreen].draw()
		howToButtons.draw()


def MenuUpdate(dt):
	MenuUpdate.t+=5*dt
	MenuUpdate.r+=2*dt
	title.y = game_window.height/2-10+5*math.sin(MenuUpdate.t)
	title.rotation = math.sin(MenuUpdate.r)
	for b in menuB1:
		b.Update()
	for b in menuB2:
		b.Update()
	for b in optB:
		b.Update()
	for b in HowToB:
		b.Update()
	if startSeq == 1 and menuB1[0].transform.position.y == -100:
		StartSeq2()
	elif startSeq == 2 and menuB2[0].transform.position.y == 200:
		StartSeq3()
	elif startSeq == 4 and menuB2[0].transform.position.y == -100:
		StartSeq5()
	elif startSeq == 5 and menuB1[0].transform.position.y == 200:
		StartSeq0()

MenuUpdate.t=0
MenuUpdate.r=0

def GameUpdate(dt):
	if draggedField is not None:
		if draggedField.rotation > destRot:
			draggedField.rotation-=1
		elif draggedField.rotation < destRot:
			draggedField.rotation+=1
	endTurnButton.Update()
	if anim is not None:
		anim.Update()


EnterMain()

pyglet.app.run()